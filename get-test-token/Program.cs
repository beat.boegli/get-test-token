﻿using Amazon.KeyManagementService;
using GetTestToken.Jwt;
using Microsoft.Extensions.Configuration;
using static System.Console;

if (Environment.GetEnvironmentVariable("KMS_SIGNATURE_KEY_ALIAS_ARN") is null)
    Environment.SetEnvironmentVariable("KMS_SIGNATURE_KEY_ALIAS_ARN", "arn:aws:kms:eu-west-1:278781821797:alias/smc-login-signing-key");


IConfigurationRoot configuration = new ConfigurationBuilder()
                                   .AddCommandLine(args)
                                   .Build();

var tenantId = configuration.GetValue("tenant", "");
var userId = configuration.GetValue("user",     "");


var genclaims = new Dictionary<string, string>
{
    ["federationId"] = "bs@federation.id",
    ["tenantId"]     = string.IsNullOrEmpty(tenantId) ? Guid.NewGuid().ToString() : tenantId,
    ["userId"]       = string.IsNullOrEmpty(userId) ? Guid.NewGuid().ToString() : userId,
};


var jwtUtils = new JwtUtils(new AmazonKeyManagementServiceClient());

var idToken = await jwtUtils.GenerateToken(genclaims, 525600);

if (idToken == null)
    Environment.Exit(1);

WriteLine($"Generate token for key: {Environment.GetEnvironmentVariable("KMS_SIGNATURE_KEY_ALIAS_ARN")}");
WriteLine("SET environment variable KMS_SIGNATURE_KEY_ALIAS_ARN before running the tool to an other key ARN to override");
foreach (var genclaim in genclaims)
{
    WriteLine($"{genclaim.Key,12} : {genclaim.Value}");
}

WriteLine();
WriteLine(idToken);