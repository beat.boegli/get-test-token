﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Amazon.KeyManagementService;
using Amazon.KeyManagementService.Model;
using Microsoft.IdentityModel.Tokens;

namespace GetTestToken.Jwt;

public interface IJwtUtils
{
    /// <summary>
    /// Generates a JWT containing the claims provided in the <paramref name="parameter" />
    /// </summary>
    /// <param name="parameter">Claims to be added to the token</param>
    /// <param name="expiryInMinutes">Set the expiry time from the current time in minutes</param>
    /// ///
    /// <returns>The JWT as a Base64URL encoded string or null</returns>
    Task<string?> GenerateToken(Dictionary<string, string> parameter, uint expiryInMinutes = 0);

    /// <summary>
    /// Validates the claims and the integrity of a JWT <br />
    /// </summary>
    /// <param name="token">The JWT as a Base64URL encoded string</param>
    /// <returns>The claims found in the token. if the token is invalid, null is returned</returns>
    Dictionary<string, string>? ValidateToken(string token);
}

/// <summary>
/// Class used for jwt functionality
/// </summary>
class JwtUtils : IJwtUtils
{
    private readonly IAmazonKeyManagementService _kms;

    private const string SigningKeyAliasConfigKey = "KMS_SIGNATURE_KEY_ALIAS_ARN";

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="kms"></param>
    public JwtUtils(IAmazonKeyManagementService kms)
    {
        _kms = kms;
    }

    /// <inheritdoc />
    public async Task<string?> GenerateToken(Dictionary<string, string> parameter, uint expiryInMinutes = 0)
    {
        var keyAlias = Environment.GetEnvironmentVariable(SigningKeyAliasConfigKey);

        if (string.IsNullOrEmpty(keyAlias))
        {
            Console.WriteLine("Key alias not configured!");
            return null;
        }


        KeyMetadata keyMetadata;

        try
        {
            //Fetch the ID of the current key by the alias
            //Used to fetch the correct key for the signature validation
            keyMetadata = (await _kms.DescribeKeyAsync(new DescribeKeyRequest
                              {
                                  KeyId = keyAlias
                              })).KeyMetadata;

            if (!keyMetadata.SigningAlgorithms.Contains(SigningAlgorithmSpec.ECDSA_SHA_256))
            {
                Console.WriteLine("Wrong signing algorithm");
                return null;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Exception during DescribeKey: {ex.Message}");
            return null;
        }

        expiryInMinutes = expiryInMinutes != 0 ? expiryInMinutes : 180;

        var tmpToken = string.Join('.', PrepareEncodedHeader(keyMetadata.KeyId, SecurityAlgorithms.EcdsaSha256), PrepareEncodedPayload(parameter, expiryInMinutes));

        SignResponse response;

        try
        {
            response = await _kms.SignAsync(new SignRequest
            {
                KeyId            = keyAlias,
                MessageType      = MessageType.RAW,
                SigningAlgorithm = SigningAlgorithmSpec.ECDSA_SHA_256,
                Message          = new MemoryStream(Encoding.ASCII.GetBytes(tmpToken))
            });
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Exception while signing: {ex.Message}");
            return null;
        }

        var signature = Base64UrlEncoder.Encode(response.Signature.ToArray());

        var token = string.Join('.', tmpToken, signature);
        return token;
    }

    /// <inheritdoc />
    public Dictionary<string, string>? ValidateToken(string token)
    {
        if (string.IsNullOrEmpty(token))
        {
            return null;
        }


        var handler = new JwtSecurityTokenHandler();

        ClaimsPrincipal claims;

        try
        {
            claims = handler.ValidateToken(token, new TokenValidationParameters
            {
                ValidateIssuer        = true,
                ValidIssuer           = "smc-login-api",
                ValidateAudience      = true,
                ValidAudience         = "supervisors",
                ValidateLifetime      = true,
                ClockSkew             = TimeSpan.Zero,
                RequireExpirationTime = true,
                SignatureValidator    = (t, _) => SignatureValidator(t).GetAwaiter().GetResult()
            }, out _);
        }
        //Catches invalid signatures
        catch (KMSInvalidSignatureException)
        {
            return null;
        }
        //Catches any exception thrown, if there is a problem with the KMS or the key
        catch (AmazonKeyManagementServiceException)
        {
            return null;
        }
        //Catches invalid tokens (formatting or otherwise)
        catch (SecurityTokenValidationException)
        {
            return null;
        }
        //Catches all remaining errors
        catch (Exception)
        {
            return null;
        }

        return claims.Claims.ToDictionary(c => c.Type, c => c.Value);
    }

    private static string PrepareEncodedHeader(string keyId, string algorithm)
    {
        var header = new JwtHeader(new SigningCredentials(new JsonWebKey(), algorithm))
        {
            { JsonWebKeyParameterNames.Kid, keyId }
        };

        return header.Base64UrlEncode();
    }

    private string PrepareEncodedPayload(Dictionary<string, string> parameter, uint validityInMinutes)
    {
        var issuer = "smc-login-api";
        var audience = "supervisors";

        var claims = parameter.Select(p => new Claim(p.Key, p.Value));

        var payload = new JwtPayload(issuer, audience, claims, DateTime.UtcNow, DateTime.UtcNow.AddMinutes(validityInMinutes), DateTime.UtcNow);

        return payload.Base64UrlEncode();
    }

    private async Task<SecurityToken> SignatureValidator(string encodedToken)
    {
        var token = new JwtSecurityToken(encodedToken);

        var message = Encoding.ASCII.GetBytes(string.Join('.', token.EncodedHeader, token.EncodedPayload));

        if (string.IsNullOrEmpty(token.Header.Alg) || !token.Header.Alg.Equals(SecurityAlgorithms.EcdsaSha256))
        {
            throw new SecurityTokenInvalidAlgorithmException($"The key was signed with an unsupported algorithm: '{token.Header.Alg}'");
        }

        var result = await _kms.VerifyAsync(new VerifyRequest
        {
            KeyId = token.Header.Kid,

            MessageType      = MessageType.RAW,
            SigningAlgorithm = SigningAlgorithmSpec.ECDSA_SHA_256,

            Message   = new MemoryStream(message),
            Signature = new MemoryStream(Base64UrlEncoder.DecodeBytes(token.RawSignature)),
        });

        if (!result.SignatureValid)
            throw new KMSInvalidSignatureException("Signature verification failed");

        return token;
    }
}